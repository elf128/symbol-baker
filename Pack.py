#!/usr/bin/python
#

from PIL import Image, ImageFont, ImageDraw, ImageOps, ImageFilter

class Pack:
    AlphaOffset = 16
    ColorOffset = 0 
    AlphaStep   = 1
    ColorStep   = 1
    
    def PackerInit():
        Pack.AlphaStart = int(pow( 2, Pack.AlphaOffset ))
        Pack.ColorStart = int(pow( 2, Pack.ColorOffset ))
        Pack.AlphaMul   = int(pow( 2, abs(Pack.AlphaStep) ))
        Pack.ColorMul   = int(pow( 2, abs(Pack.ColorStep) ))
        print( "%04x"%Pack.AlphaStart )
        print( "%04x"%Pack.ColorStart )
        print( "%d"%Pack.AlphaMul )
        print( "%d"%Pack.ColorMul )

    def __init__( self ):
        self.BITS = 0
        self.AlphaMask = Pack.AlphaStart
        self.ColorMask = Pack.ColorStart

    def pushL( self, L ):
        self.BITS += L * self.ColorMask
        if Pack.ColorStep>0:
            self.ColorMask  *= self.ColorMul 
        if Pack.ColorStep<0:
            self.ColorMask   = int( self.ColorMask / self.ColorMul ) 
    
    def pushA( self, A ):
        self.BITS += A * self.AlphaMask
        if Pack.AlphaStep>0:
            self.AlphaMask  *= self.AlphaMul 
        if Pack.AlphaStep<0:
            self.AlphaMask   = int( self.AlphaMask / self.AlphaMul ) 
    
    def write( self, File, bytes, bigByteFirst ):
        if bigByteFirst:
            File.write( self.BITS.to_bytes(bytes, 'big') )
        else:
            File.write( self.BITS.to_bytes(bytes, 'little') )
