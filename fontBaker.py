#!/usr/bin/python
#

from PIL import Image, ImageFont, ImageDraw, ImageOps, ImageFilter

from Glyphs import GlyphGen
from Chars  import CharGen, Thresholds
from Pack   import Pack
 
import sys, os, math

debug        = 0

OpaqueOffset = 16
WhiteOffset  = 0 
Step         = 1
#storeAlpha   = 1


if __name__=="__main__":
    if len(sys.argv)<2:
        print( "Usage: %s -fn=(font name) -cw=(char width) -ch=( char height ) -fs=( font size ) -d=( depth )  ( bin file )"%(sys.argv[0]) )
        exit(-1)

    params = dict()
    Bin    = ""
    
    fontName        = "FreeMono"
    charWidth       = 16
    charHeight      = 24
    fontSize        = 20
    binContrast     = 16
    dL              = 8
    dA              = 8
    distanceField   = False
    outlineSize     = 2
    outlineBlur     = 1
    adds            = ""
    charCount       = 128
    verticalShift   = 0
    horisontalShift = 0
    zoom            = 16
    drawGrid        = 1
    
    rowPacking      = True
    linePacking     = False
    invY            = False
    invX            = False
    packData        = True
    bytePerPack     = 2
    pixPerPack      = 16
    interleaving    = False
    splitDepth      = False
    bigByteFirst    = False
    
    interAlphaSt    = 0
    interLumaSt     = 0
    interAlphaMove  = 0
    interLumaMove   = 0

    sampleText = "The quick brown fox jumps over the lazy dog. 0123456789:,;(*!?'/\\\")$%^&-+@~#<>{}[]\x80\x81\x82\x83\x84\x85"
    
    for i in range(1,len(sys.argv)):
        A = sys.argv[ i ]
        if A[0] == '-':
            #parameter
            p = A.split("=")
            params[ p[0][1:] ] = p[1]
            try:
                if   p[0] == "-fn":
                    fontName = p[1]
                elif p[0] == "-cw":
                    charWidth = int(p[1])
                elif p[0] == "-ch":
                    charHeight = int(p[1])
                elif p[0] == "-fs":
                    fontSize = int(p[1])
                elif p[0] == "-d":
                    D  = p[1].split( ":" )
                    dL = int( D[0] )
                    if len(D)>1:
                        dA = int( D[1] )
                    else:
                        dA = 0
                        
                elif p[0] == "-df":
                    distanceField = bool(int(p[1]))
                elif p[0] == "-os":
                    outlineSize = int(p[1])
                elif p[0] == "-ob":
                    outlineBlur = float(p[1])
                elif p[0] == "-ad":
                    adds = int(p[1])
                elif p[0] == "-cc":
                    charCount = int(p[1])
                elif p[0] == "-rp":
                    rowPacking = bool(int(p[1]))
                elif p[0] == "-lp":
                    linePacking = bool(int(p[1]))
                elif p[0] == "-iy":
                    invY = bool(int(p[1]))
                elif p[0] == "-ix":
                    invX = bool(int(p[1]))
                elif p[0] == "-pd":
                    packData = bool(int(p[1]))
                elif p[0] == "-bpp":
                    bytePerPack = int(p[1])
                elif p[0] == "-il":
                    interleaving = bool(int(p[1]))
                elif p[0] == "-sd":
                    splitDepth = bool(int(p[1]))
                elif p[0] == "-bbf":
                    bigByteFirst = bool(int(p[1]))
                elif p[0] == "-bc":
                    binContrast = int(p[1])
                elif p[0] == "-vs":
                    verticalShift = float(p[1])
                elif p[0] == "-hs":
                    horisontalShift = float(p[1])
                elif p[0] == "-z":
                    zoom = int(p[1])
                elif p[0] == "-dg":
                    drawGrid = int(p[1])
                elif p[0] == "-st":
                    sampleText = p[1]
            except ValueError as V:
                print("\n\nError in parameter %d (%s)\n%s" %( i, p[0], V) )
                print("arguments: %s"%(" ".join( sys.argv ) ) )
                exit(-1)
        else:
            #file name
            if Bin == "":
                Bin = A
            else:
                print("\n\nError in parameters.\nMultiple filenames. Filename '%s' was found, while '%s' have been defined already"%(A,Bin))
                exit(-1)


    ###
    #
    bytePerPack  = min( 4, bytePerPack )
    pixPerPack   = rowPacking and charWidth or charHeight
    
    zoom         = max( 1, zoom )
    if zoom == 1:
        drawGrid = 0
    if interleaving == False:
        interLumaMove  = dL
        interAlphaMove = dA
    else:
        interLumaMove  = dL+dA
        interAlphaMove = dA+dL
        interLumaSt    = 0
        interAlphaSt   = dL
        
    fontText   = "".join([ chr(i) for i in range(32,32+min(96,charCount)) ] )
    #fontText   = " !138@"
    charCount  = len( fontText )
    # One more time, cause user can request other settings for interleaving
    #
    for i in range(1,len(sys.argv)):
        A = sys.argv[ i ]
        if A[0] == '-':
            p = A.split("=")
            params[ p[0][1:] ] = p[1]

            if   p[0] == "-ias":
                interAlphaSt = int(p[1])
            elif p[0] == "-ils":
                interLumaSt = int(p[1])
            elif p[0] == "-iam":
                interAlphaMove = int(p[1])
            elif p[0] == "-ilm":
                interLumaMove = int(p[1])
            elif p[0] == "-ppp":
                pixPerPack = int(p[1])

    
    if dA == 0:
        # if we're not writing alpha, we don't have to make separate pack for it.
        # Interleaving will force using single pack
        interleaving = True

    if Bin == "":
        print("Output filename is not defined :(")
        Bin = "default_sample.png"
        
    print("Building font with following settings:")
    print("\nFont settings:")
    print("..fontName        (-fn)  = '%s'"% fontName )
    print("..charWidth       (-cw)  = %d"  % charWidth)
    print("..charHeight      (-ch)  = %d"  % charHeight)
    print("..fontSize        (-fs)  = %d"  % fontSize)
    print("..outlineSize     (-os)  = %d"  % outlineSize)
    print("..outlineBlur     (-ob)  = %.3f"  % outlineBlur)
    print("..binContrast     (-bc)  = %d"  % binContrast)
    print("..verticalShift   (-vs)  = %.3f  ( negative numbers will cause shift up )"  % verticalShift)
    print("..horisontalShift (-hs)  = %.3f  ( negative numbers will cause shift left )"  % horisontalShift)
    print("\nDepth and additional graphics settings:")
    print("..depth           (-d)   = %d:%d  ( %d bit%s of luma and %d bit%s of alpha )"  % ( 
        dL, dA, dL, dL!=1 and "s" or "", dA, dA!=1 and "s" or "" ) )
    print("..distanceField   (-df)  = %s ( first channel stores %s  )"  % (
         distanceField, distanceField and "distance field" or "luminosity" )
          )
    print("..adds            (-ad)  = '%s'"% adds )
    print("..charCount       (-cc)  = %d  ( How many characters will be exported )"% charCount )
    print("..sampleText      (-st)  = %s" % sampleText )
    print("..fontText        {n/a}  = %s" % fontText )
    print("..zoom            (-z)   = %s" % zoom )
    print("..drawGrid        (-dg)  = %s" % drawGrid )

    print("\nExport settings:")
    print("..rowPacking      (-rp)  = %s ( pixels of character are packed %s )"  % (
         rowPacking, rowPacking and "horizontally" or "vertically" )
          )
    print("..linePacking     (-lp)  = %s ( font is stored %s )"  % (
         linePacking,
         linePacking and "line after line. All first lines of all characters, then all second and so on" or
                         "char after char. All lines of first character, then all lines of the second and so on" ))
    print("..invX            (-ix)  = %s ( horizontal storing order is '%s first' )"  % (
         invX, invX and "right" or "left" )
          )
    print("..invY            (-iy)  = %s ( vertical storing order is '%s first' )"  % (
         invY, invY and "bottom" or "top" )
          )

    print("..packData        (-pd)  = %s ( %s )"  % (
         packData, packData and "multiple pixels will be packed together" or 
                                "each pixel stored separately" )
          )
    print("..bytePerPack     (-bpp) = %s"  % ( bytePerPack ))
    print("..pixPerPack      (-ppp) = %s ( How many pixels will be packed together )"  % ( pixPerPack  ))
          
    print("..interleaving    (-il)  = %s ( alpha information %s )"  % (
         interleaving, interleaving and "is packed with color info" or "is stored after color info in separate pack" )
          )
    print("..splitDepth      (-sd)  = %s ( bits of the same pixel are stored %s  )"  % (
         splitDepth, splitDepth and "in separate packs" or "together as a single value" )
          )
    print("..bigByteFirst    (-bbf) = %s ( big byte of a pack stored %s  )"  % (
         bigByteFirst, bigByteFirst and "first" or "last" )
          )
    print("\nInterleaving settings")
    print("..interleaving    (-il)  = %s ( alpha information %s )"  % (
         interleaving, interleaving and "is packed with color info" or "is stored after color info in separate pack" )
          )
    print("..This is a very powerful thing. It allow to interleave data any way you can imagine, ")
    print("..However! Messing around following settings can AND WILL lead to massive headache, loss of sleep and random acts off aggression")
    print("..Use it wisely.")
    print("..interLumaSt     (-ils) = %d ( luma information will start from this bit )"  % ( interLumaSt ) )
    print("..interAlphaSt    (-ias) = %d ( alpha  information will start from this bit )"  % ( interAlphaSt ) )
    print("..interLumaMove   (-ilm) = %d ( each next storing position for luma will be this many bits left. Yes it can be negative )"  % ( interLumaMove ) )
    print("..interAlphaMove  (-iam) = %d ( each next storing position for alpha will be this many bits left. Yes it can be negative )"  % ( interAlphaMove ) )

    print("..OutputFile   = '%s'"% Bin )

    if packData:
        if interleaving:
            if ( bytePerPack * 8 ) < ( (dA + dL)*pixPerPack ):
                print("\n\nError: Data will not fit in pack")
                print("  packsize is %d bits"%(bytePerPack*8) )
                print("  data is (luma+alpha)×pixels per pack")
                print("  ( %d + %d ) × %d = %d bits" % ( dL, dA, pixPerPack, ( dL + dA ) * pixPerPack ) ) 
                exit(-1)
                 
    print("\n-----------------------------------")
    print("...making Glyphs")
    Glyphs = GlyphGen.makeGlyphs( fontName,
                                  charWidth,
                                  charHeight,
                                  fontSize,
                                  verticalShift,
                                  horisontalShift,
                                  fontText,
                                  adds )
        
    Thresholds.binContrast = binContrast
    Thresholds.setGamma( binContrast )
    
    Chars = CharGen.makeChars( Glyphs, fontText, dL, dA, outlineSize, outlineBlur, distanceField )
    
    ext = Bin.split(".")[-1]
    
    if ext in ["bin","font_bin", "font1_bin", "font2_bin", "font11_bin", "font22_bin" ]:
        print("...making binary.")
        B = open( Bin, "wb" )
        
        for ch in range( charCount ):
            Chars[ ch ] = Chars[ ch ].load()

#    pixPerPack   = 16
#    splitDepth   = False

        axisLen1 = 0
        axisLen2 = 0
        axisLen3 = 0
        y  = 0
        x  = 0
        ch = 0
        
        Pack.AlphaOffset = interAlphaSt
        Pack.ColorOffset = interLumaSt
        Pack.AlphaStep   = interAlphaMove
        Pack.ColorStep   = interLumaMove
        Pack.PackerInit()
        
        if rowPacking:
            if linePacking:
                # x, ch, y
                axisLen1 = charWidth
                axisLen2 = charCount
                axisLen3 = charHeight
            else:
                # x, y ,ch
                axisLen1 = charWidth
                axisLen2 = charHeight
                axisLen3 = charCount
        else:
            if linePacking:
                # y, ch, x
                axisLen1 = charHeight
                axisLen2 = charCount
                axisLen3 = charWidth
            else:
                # y, x, ch
                axisLen1 = charHeight
                axisLen2 = charWidth
                axisLen3 = charCount
                
        
        for axis3 in range( axisLen3 ):
            if linePacking:
                if rowPacking:
                    if invY: y = (charHeight-axis3-1)
                    else:    y = axis3
                else:
                    if invX: x = (charWidth -axis3-1)
                    else:    x = axis3
            else:
                ch = axis3
                
            #######################
            #
            for axis2 in range( axisLen2 ):
                if linePacking:
                    ch = axis2
                else:
                    if rowPacking:
                        if invY: y = (charHeight-axis2-1)
                        else:    y = axis2
                    else:
                        if invX: x = (charWidth -axis2-1)
                        else:    x = axis2
                    
                if packData:
                    packL = Pack()
                    if interleaving == False:
                        packA = Pack()
                
                #######################
                #
                for axis1 in range( axisLen1 ):
                    if rowPacking:
                        if invX: x = (charWidth -axis1-1)
                        else:    x = axis1
                    else:
                        if invY: y = (charHeight-axis1-1)
                        else:    y = axis1
                    
                    if not packData:
                        packL = Pack()
                        if interleaving == False:
                            packA = Pack()
                    
                    #######################
                    #
                    color, _, _, alpha = Chars[ch][ x, y ]
                    
                    packL.pushL( color )
                    if interleaving == False:
                        packA.pushL( alpha )
                    else:
                        if dA:
                            packL.pushA( alpha )
                    
                    if not packData:
                        packL.write( B, bytePerPack, bigByteFirst )
                        if interleaving == False:
                            packA.write( B, bytePerPack, bigByteFirst )

                    
                if packData:
                    packL.write( B, bytePerPack, bigByteFirst )
                    if interleaving == False:
                        packA.write( B, bytePerPack, bigByteFirst )
    else:
        print( '...output file is not binary, making sample image instead')
        
        sample = Image.new( "LA", ( charWidth * len( sampleText ), charHeight ),0 )
        
        carret = 0
        for ch in sampleText:
            sample.paste( Chars[ min( ord( ch )- 32, charCount-1 ) ], (carret,0 ) )
            carret += charWidth
        
        zoomed = Image.new( "RGBA", ( sample.size[0]*zoom , sample.size[1]*zoom ),0 )
        
        draw  = ImageDraw.Draw( zoomed )
        img   = sample.load()
        
        for x in range( sample.size[0] ):
            for y in range( sample.size[1] ): 
                L,A = img[x,y]
                if x==3 and y==0:
                    print( L, A )
                    
                L = int( (L*255.0) / ( pow( 2, dL ) - 1 ) )
                if dA:
                    A = int( (A*255.0) / ( pow( 2, dA ) - 1 ) )
                else:
                    A = 255
                    
                if x==3 and y==0:
                    print( L, A )
                    
                L2= int(L*0.5)
                L3= int(L*0.75)
                
                if rowPacking:
                    if linePacking:
                        # x, ch, y
                        cx = (L2   ,L2   ,L2+128,255)
                        cy = (L3+64,L3   ,L3    ,255)
                        cc = (L3   ,L3+64,L3    ,255)
                    else:
                        # x, y ,ch
                        cx = (L2   ,L2   ,L2+128,255)
                        cy = (L3,   L3+64,L3    ,255)
                        cc = (L3+64,L3   ,L3    ,255)
                else:
                    if linePacking:
                        # y, ch, x
                        cx = (L3+64,L3   ,L3    ,255)
                        cy = (L2   ,L2   ,L2+128,255)
                        cc = (L3   ,L3+64,L3    ,255)
                    else:
                        # y, x, ch
                        cx = (L3   ,L3+64,L3    ,255)
                        cy = (L2   ,L2   ,L2+128,255)
                        cc = (L3+64,L3   ,L3    ,255)
                if drawGrid:
                    draw.rectangle( ((x*zoom+1,y*zoom+1), (x*zoom+zoom-1,y*zoom+zoom-1)), fill=(L,L,L,A))
                    draw.line( ((x*zoom+0,y*zoom+0),(x*zoom+zoom-1,y*zoom+0)), fill=cx )
                    draw.line( ((x*zoom+0,y*zoom+1),(x*zoom+0,y*zoom+zoom-1)), fill=cy )
                else:
                    draw.rectangle( ((x*zoom+0,y*zoom+0), (x*zoom+zoom-1,y*zoom+zoom-1)), fill=(L,L,L,A))

                
            if drawGrid and (int(x/charWidth)*charWidth) == x:
                draw.line( ((x*zoom, 0),(x*zoom,sample.size[1]*zoom-1 )), fill=cc, width=3 )
        zoomed.save( Bin )
    print("done.")

