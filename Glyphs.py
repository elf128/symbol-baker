#!/usr/bin/python
#

from PIL import Image, ImageFont, ImageDraw, ImageOps, ImageFilter

debug        = 3

class GlyphGen:
    def symbolGen8x( fontName, width, height, fontsize, verticalShift, horisontalShift, char ):
        glyph    = Image.new( "L", (width*8, height*8), 0 )
    
        draw     = ImageDraw.Draw( glyph )
        font     = ImageFont.truetype( fontName, fontsize*8 )
        charSize = font.getsize( char )
        
        offset = width*8 - charSize[0]
        if offset < 0:
            print( "Warning: symbol %s is wider than provided font area. "%(char) ) 
        if charSize[1] > height*8:
            print( "Warning: symbol %s is higher than provided font area. "%(char) ) 
        #print( "size %d, %d"%  )
        draw.text( (offset/2+horisontalShift, verticalShift ), char, 255, font=font )
    
        glyph = ImageOps.autocontrast( ImageOps.posterize( glyph, 1 ) )
        if debug>2:
            glyph.save( "debug/char_%2x_x8.png"%( ord(char) ) )
            
        return glyph
    
    def additional( dir, glyphs, width, height ):
        lastGlyph = len( glyphs )
        if dir != "":
            # Nothing to add
            files = os.listdir( dir )
            ADDS = [Image.open( dir + "/" + s ) for s in files ]
            for i in range( len( ADDS ) ):
                size   = ADDS[i].size
                tile   = ( math.ceil( size[0]/( width * 8.0 ) ), math.ceil( size[1]/( height * 8.0 ) ) )
                offset = ( int(( tile[0] * ( width * 8 ) - size[0] )/2) ,int(( tile[1] * ( height * 8 ) - size[1] )/2) )
                New = Image.new( "LA", ( int( tile[0] * width * 8 ), int( tile[1] * height * 8 ) ) , 0 )
                New.paste( ADDS[i], offset )
                print( files[i], size, tile, offset )
                for y in range( tile[1] ):
                    for x in range( tile[0] ):
                        slice = New.crop( ( x * width * 8, y * height * 8, (x+1) * width * 8, (y+1) * height * 8 ))
                        slice.load()
                        glyphs.append( slice )
                        lastGlyph += 1
                        
        for i in range( lastGlyph, 128 ):
            glyphs.append( glyphs[0] )
            
    def makeGlyphs( fontName, charWidth, charHeight, fontSize, verticalShift, horisontalShift, fontText, adds ):
        Glyphs = [ GlyphGen.symbolGen8x( fontName,
                                          charWidth,
                                          charHeight,
                                          fontSize,
                                          verticalShift*8.0,
                                          horisontalShift*8.0,
                                          i ) for i in fontText ]
    
        GlyphGen.additional( adds, Glyphs, charWidth, charHeight )
        return Glyphs

