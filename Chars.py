#!/usr/bin/python
#

from PIL import Image, ImageFont, ImageDraw, ImageOps, ImageFilter

import math

from multiprocessing.dummy import Pool as ThreadPool

thread_count = 12

class Thresholds:
    binContrast  = 16
    
    def setGamma( mid, debug = 0 ):
        Thresholds.gamma  = math.log(0.5) / math.log( mid/64 )
        if debug == 1:
            print("GammaTest:")
            print("\n".join([
                (
                    "%2d -> %2d, %2d, %2d, %2d"%( i,
                                                  Thresholds.MultiBit( i, 1 ),
                                                  Thresholds.MultiBit( i, 2 ),
                                                  Thresholds.MultiBit( i, 4 ),
                                                  Thresholds.MultiBit( i, 5 ) )
                ) for i in range(0,65,2) ]))
        
    def MultiBit(input, bits ):
        if 1:
            return int( pow(input/65.0,Thresholds.gamma)*( pow( 2, bits ) ) )
        else:
            if bits == 2:
                return Thresholds.Quadrilian(input)
            elif bits == 4:
                return int(input/4 )*16;
            elif bits == 5:
                return int(input/2 )*8;
            elif bits >= 6:
                return input * 4;
            else:
                return (input > Thresholds.binContrast) and 255 or 0
        
class CharGen:
    def makeChar( glyph, depth, mode ):
        size = glyph.size
        size = ( int(size[0]/8), int(size[1]/8 ) )
        BIT = Image.new( mode, size )
        
        pix = BIT.load()
        for y in range( size[1] ):
            for x in range( size[0] ):
                cut = glyph.crop(( x*8, y*8, (x+1)*8, (y+1)*8 ) )
                hist = cut.histogram( )[-1]
                #print( "%d, %d = %s"%(x,y, hist ) )
                pix[ x, y ] = Thresholds.MultiBit( hist, depth ) 
                
        return BIT

    def _selectIndirect_( offset ):
        ignore = [ False, False, False, False, False, False, False, False ]
        
        for i in range ( 7 ):
            if not ignore[i]: 
                for j in range( i+1, 8 ):
                    if ( offset[ i ] == offset[ j ] ):
                        #print ( "%d == %d"%( i,j ) )
                        ignore[ j ] = True
                    
        D = 128*128 + 128*128
        m0 = 0
        m1 = 0
        for i in range ( 8 ):
            if not ignore[i]:
                P = offset[ i ]
                d = P[0] * P[0] + P[1] * P[1]
                if d < D:
                    D  = d
                    m0 = i

        D = 128*128 + 128*128
        for i in range ( 8 ):
            if not ignore[i] and i != m0:
                P = offset[ i ]
                d = P[0] * P[0] + P[1] * P[1]
                if d < D:
                    D  = d
                    m1 = i

        return ( offset[ m0 ][ 0 ]+128,
                 offset[ m0 ][ 1 ]+128,
                 offset[ m1 ][ 0 ]+128,
                 offset[ m1 ][ 1 ]+128 )
    

    def _indirect( dx, dy, pix ):
        if ( pix[ 0 ] == 0 and pix[ 1 ] == 0 ):
            x1 = 0
            y1 = 0
        else:
            x1 = pix[ 0 ] + dx
            y1 = pix[ 1 ] + dy
            
        if ( pix[ 2 ] == 0 and pix[ 3 ] == 0 ):
            x2 = 0
            y2 = 0
        else:
            x2 = pix[ 2 ] + dx
            y2 = pix[ 3 ] + dy 
        
        return ( x1, y1, x2, y2 )
    
    def _DistStep( FB, BB, size, debugOut ):
        step = 1
            
        for y in range( size[1] ):
            for x in range( size[0] ):
                Cur = FB[ x, y ] 
                if ( Cur[ 0 ] != 255 or 
                     Cur[ 1 ] != 255 or 
                     Cur[ 2 ] != 255 ):
                    # Empty
                    nX = (   x > 0 )       and FB[ x - 1, y + 0 ] or (0,0,0,0)
                    pX = ( x+1 < size[0] ) and FB[ x + 1, y + 0 ] or (0,0,0,0)
                    nY = (   y > 0 )       and FB[ x + 0, y - 1 ] or (0,0,0,0)
                    pY = ( y+1 < size[1] ) and FB[ x + 0, y + 1 ] or (0,0,0,0)
                    
                    #direct hit diagonals
                    if   ( nX[0] == 255 and nX[1] == 255 and nX[2] == 255  and 
                           nY[0] == 255 and nY[1] == 255 and nY[2] == 255 ):
                        BB[x,y] = ( 128 - step, 128, 128, 128 - step )
                    elif ( pX[0] == 255 and pX[1] == 255 and pX[2] == 255 and
                           nY[0] == 255 and nY[1] == 255 and nY[2] == 255 ):
                        BB[x,y] = ( 128 + step, 128, 128, 128 - step )
                    elif ( nX[0] == 255 and nX[1] == 255 and nX[2] == 255  and 
                           pY[0] == 255 and pY[1] == 255 and pY[2] == 255 ):
                        BB[x,y] = ( 128 - step, 128, 128, 128 + step )
                    elif ( pX[0] == 255 and pX[1] == 255 and pX[2] == 255 and
                           pY[0] == 255 and pY[1] == 255 and pY[2] == 255 ):
                        BB[x,y] = ( 128 + step, 128, 128, 128 + step )

                    #direct hit one side 
                    elif   ( nX[0] == 255 and nX[1] == 255 and nX[2] == 255 ):
                        BB[x,y] = ( 128 - step, 128,      0, 0 )
                    elif ( pX[0] == 255 and pX[1] == 255 and pX[2] == 255 ):
                        BB[x,y] = ( 128 + step, 128,      0, 0 )
                    elif ( nY[0] == 255 and nY[1] == 255 and nY[2] == 255 ):
                        BB[x,y] = ( 128,        128-step, 0, 0 )
                    elif ( pY[0] == 255 and pY[1] == 255 and pY[2] == 255 ):
                        BB[x,y] = ( 128,        128+step, 0, 0 )
                    else:
                        if debugOut and x == 44 and y == 31:
                            print ( nX )
                            print ( pX )
                            print ( nY )
                            print ( pY )
                            
                        if ( nX[0] > 0 or nX[1] > 0 or nX[2] > 0 ):
                            #direct hit
                            nX = CharGen._indirect( -step,  0, nX )
                        else:
                            nX = ( 0, 0, 0, 0 )
                            
                        if ( pX[0] > 0 or pX[1] > 0 or pX[2] > 0 ):
                            #direct hit
                            pX = CharGen._indirect( +step,  0, pX )
                        else:
                            pX = ( 0, 0, 0, 0 )
                            
                        if ( nY[0] > 0 or nY[1] > 0 or nY[2] > 0 ):
                            #direct hit
                            nY = CharGen._indirect(  0, -step, nY )
                        else:
                            nY = ( 0, 0, 0, 0 )
                            
                        if ( pY[0] > 0 or pY[1] > 0 or pY[2] > 0 ):
                            #direct hit
                            pY = CharGen._indirect(  0, +step, pY )
                        else:
                            pY = ( 0, 0, 0, 0 )
                            
                        O = [
                                ( nX[0]-128, nX[1]-128 ),
                                ( nX[2]-128, nX[3]-128 ),
                                ( pX[0]-128, pX[1]-128 ),
                                ( pX[2]-128, pX[3]-128 ),
                                ( nY[0]-128, nY[1]-128 ),
                                ( nY[2]-128, nY[3]-128 ),
                                ( pY[0]-128, pY[1]-128 ),
                                ( pY[2]-128, pY[3]-128 )
                            ]
                        
                        
                        if debugOut and x == 44 and y == 31:
                            print( "\nBefore select")
                            print ( "\n".join( "%s\t = %.2f"%(str( s ), (s[0])*(s[0])+(s[1])*(s[1]) ) for s in O ) )
                            
                        S = CharGen._selectIndirect_( O )
                        
                        if debugOut and x == 44 and y == 31:
                            print( "\nAfter select")
                            print ( S )
                        
                        BB[x,y] =  S
                else:
                    BB[x,y] = Cur
    
    def makeDist( glyph, depth, debugOut ):
        iterations = 32
        size = glyph.size
        print("Dist")
        #size = ( int(size[0]/8), int(size[1]/8 ) )
        BIT0 = glyph.convert(mode="RGBA")
        BIT1 = BIT0.copy()
        
        FB   = BIT0.load()
        BB   = BIT1.load()
        
        for i in range( iterations ):
            CharGen._DistStep( FB, BB, size, debugOut )
            t  = FB
            FB = BB
            BB = t
            #if debugOut and i == 2:
            #    if i%2 == 0:
            #        BIT1.show();
            #    else:
            #        BIT0.show();
                
        for y in range( size[1] ):
            for x in range( size[0] ):
                a,b,c,_ = FB[ x, y ]
                if a != 255 or b != 255 or c != 255:
                    d = math.sqrt( (a-128)*(a-128) + (b-128)*(b-128) )
                    BB[ x, y ] =  (a,b,int(d*8),255)
                    FB[ x, y ] =  (255,255,255,255)
                else:
                    FB[ x, y ] =  ( 0, 0, 0, 0 )

        outer = BB
        BIT2  = BIT0.copy()
        BB    = BIT2.load()
        
        for i in range( 16 ):
            CharGen._DistStep( FB, BB, size, debugOut )
            t  = FB
            FB = BB
            BB = t
        
        for y in range( size[1] ):
            for x in range( size[0] ):
                a,b,c,_ = FB[ x, y ]
                L = 0
                if a != 255 or b != 255 or c != 255:
                    d = math.sqrt( (a-128)*(a-128) + (b-128)*(b-128) )
                    L = int(128-d*4)
                    #BB[ x, y ] =  (0,int(128-d*4),int(d*8),255)
                #    FB[ x, y ] =  (255,255,255,255)
                else:
                    L = int(124+outer[ x, y ][2]/2)
                    #BB[ x, y ] =  ( outer[ x, y ][2], int(127+outer[ x, y ][2]/2), 0, 255 )

                BB[ x, y ] =  ( L, L, L, 255 )
                   
        BIT = BIT2.resize( ( int(size[0]/8), int(size[1]/8) ), Image.ANTIALIAS )
        
        BIT.save("debug/Dist.png");
        #BIT2.save("debug/Dist_in.png");

        return BIT

    def makeChars( glyphs, fontText, dL, dA, outlineSize, outlineBlur, distField ):
        Chars  = []
        params = []
        for i in range(len(fontText)):
            params.append( (
                    glyphs[i],
                    fontText[i],
                    dL,
                    dA,
                    outlineSize,
                    outlineBlur,
                    distField,
    #                ( i == ( len(fontText)-1 ) )
                    0
                    ) )
        for i in params:
            Chars.append( CharGen.makeNbit( i ) )
            
        return Chars
    
    def makeChars_MT( glyphs, fontText, dL, dA, outlineSize, outlineBlur, distField ):

        pool = ThreadPool( 1 )
        
        params = []
        for i in range(len(fontText)):
            params.append( (
                    glyphs[i],
                    fontText[i],
                    dL,
                    dA,
                    outlineSize,
                    outlineBlur,
                    distField,
    #                ( i == ( len(fontText)-1 ) )
                    0
                    ) )
                
        Chars = pool.map( CharGen.makeNbit, params )
        
        pool.close()
        pool.join()
        
        return Chars
    
    def makeNbit( Params ):
        glyph, char, dL, dA, filer1, filter2, distField, debugOut = Params
        
        if dL == 1 or dA != 0:
            BIT = CharGen.makeChar( glyph, 1, 'L' )
        
        if dL > 1:
            if distField == False:
                NICE = CharGen.makeChar( glyph, dL, 'L' )
            else:
                NICE = CharGen.makeDist( glyph, dL, debugOut )
                #ImageList.append( NICE )
                return NICE
            
        if dA != 0:
            ALPHA = BIT.filter( ImageFilter.MaxFilter( 1+filer1*2 ) )
            if dA > 1:
                ALPHA = ImageOps.autocontrast(ALPHA).filter( ImageFilter.GaussianBlur( filter2 ) )
                ALPHA = ImageOps.autocontrast( ImageOps.posterize( ALPHA, dA ) )
                ALPHA = Image.eval( ALPHA, lambda a: int((a*pow(2,dA)-1)/255) )
                
        else:
            size = glyph.size
            size = ( int(size[0]/8), int(size[1]/8 ) )
            ALPHA = Image.new( 'L', size, 255 )
            
        if dL > 1:
            BIT = NICE
            
        #ImageList.append( Image.merge( "RGBA", ( BIT,BIT,BIT, ALPHA ) ) )
        return Image.merge( "RGBA", ( BIT,BIT,BIT, ALPHA ) )
