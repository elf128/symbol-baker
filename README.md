# Bare minimum

## Requirements

Symbol Baker is written in Python and it require one to run. So it's practicaly OS agnostic and should run just fine on Linux, Mac or Win. However, since it use fonts installed in the system your options will be a bit different.

What you need to have to run it:
 * Python3,
 * PIL,
 * Clone of this repository,
 * Common sense and basic knowledge about console applications.

Check out project's Wiki for more info:
https://gitlab.com/elf128/symbol-baker/wikis/home

## First run

Symbol backer can run in two modes.
 * Sample render,
 * Binary render,

Sample render make sample png file with a sample text using current settings.

This mode is good to experiment with settings and adjust all numbers and tweaks. It gives you an idea of how your font will look like on the target device. Also it add guidance to help you understand how data will be ordered in the binary.

The file you have to run is ***fontBaker.py***. A good start will be to use only one parameter, filename for the sample you what to build. Use some .png filename as the start.

Once you run it It will give you a huge list of settings and it will render a sample png file. You can change almost any of the settings by adding additional parameters.

For instance to render into *mySample.png* and use font size 10 you have to add **-fs=10** and filename

```
python3 fontBaker.py -fs=10 mySample.png
```


